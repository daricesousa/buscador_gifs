import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:buscador_gifs/main.dart';

Future<Map> pegarGifs() async {
  http.Response resposta;
  if (pesquisa == '') {
    resposta = await http.get(
        "https://api.giphy.com/v1/gifs/trending?api_key=9Ux2XSn95zbIa9Ywo5bYvUddZjhlsqYc&limit=16&rating=g");
  } else {
    resposta = await http.get(
        "https://api.giphy.com/v1/gifs/search?api_key=9Ux2XSn95zbIa9Ywo5bYvUddZjhlsqYc&q=$pesquisa&limit=13&offset=$pagina&rating=g&lang=pt");
  }
  return json.decode(resposta.body);
}

int pegarCount(x) {
  if (pesquisa == '') {
    return x.length;
  } else {
    return x.length + 1;
  }
}

mudarPagina({context, pagina}) {
  Navigator.push(context, MaterialPageRoute(builder: (context) {
    return pagina;
  }));
}

bool pertenceFavoritos(String x) {
  for (int i = 0; i < favoritos.length; i++) {
    if (favoritos[i]['gif']['id'] == x) {
      return true;
    }
  }
  return false;
}
